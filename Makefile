TEX = pdflatex -interaction nonstopmode

PAPER := $(shell find main*.tex | tr "\n" " ")
TARGET := $(shell find main*.tex | sed "s/tex/pdf/" | tr "\n" " ")


all: $(PAPER)
	for o in $(PAPER) ; do \
		$(TEX) $$o ; \
		$(TEX) $$o ; \
		done

view: $(PAPER)
	open $(PAPER)

spell::
	ispell *.tex

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof $(TARGET)
